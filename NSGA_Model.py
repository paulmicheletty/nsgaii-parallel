"""
NSGA-II Application to SAC-SNOW Model Calibration

Jonathan A. Quebbeman
May, 2016

To initialize the profile:
. /projects/NASA/calb/bin/do_nasa

"""
from numpy import *
from random import choice
from copy import *
from fpconst import *
from collections import OrderedDict
from Individual import *
from sys import exit
from datetime import datetime

import subprocess #For shelling models
import os as ops
#JAQ Functions:
from Fitness import * 
#from UpdateFiles import *
#load function to convert NWS file to timeseres:
from NWS2TS import NWS2TS # used to convert NWS cards to readable timeseries 
from mcp3mod import DeckMod #used to update deck file with new parameter set

subprocess.call('. /projects/NASA/calb/bin/do_nasa',shell=True)

#__all__ = ['Problem', 'ProblemModel']

"""
This class defines the problem in all details needed to run the experiment.
"""

#Initialize some record keeping files; write erases over existing file
open('fitness.txt','w')
open('genes.txt','w')
open('genes_unnorm.txt','w')
open('iteration.txt','w')
Files=ops.listdir(ops.curdir)
RMFiles=[Fi for Fi in Files if Fi.startswith('front') or Fi.startswith('Decision')]
for Fi in RMFiles:
    ops.remove(Fi)

#JAQ Notes to Check Files:
# Update BasinName descriptor
# 
global iteration
################################
class Problem(object):
    '''
    To DO:
    - variable BasinName option
    - read dictionary or external file for ActivePars
    - read in fixed values 
    '''
    DEBUG=False
   
    print "Initializing NSGAII Parameters"
    iteration=0 #starting
    #N = 50 # Size of population [F] 25
    #n = 17 ##  variables
    M = 3  # Number of objective functions
    #G = 30 # Number of generations (iterations) in experiment 20
    
    eta_c = 3. # Crossover distribution 20 
    eta_m = 2. # Mutation distribution 20 (larger =flatter Delta during mutation)
    var_bounds = [0.0, 1.0] # Fraction between Min and Max value
    #--------------------------------------
    # Load parameter min and max limits into dictionary
    # genes are limited by range, whereas these limits are used when updating written parameter files
    Limits=genfromtxt('./params/param_limits.txt',delimiter='\t',\
    dtype=("|S6"), autostrip=True,comments="#")
    Max=[float(V) for V in Limits[:,1]]
    Min=[float(V) for V in Limits[:,2]]
    VMax=dict(zip(Limits[:,0],Max))
    VMin=dict(zip(Limits[:,0],Min))
    #--------------------------------------
    "Variables for experiment"
    P = [] # Population
    
    "Initialize the problem for experiment"
    def __init__(self,N,G,BasinName):
        #passed in from __main__
        self.N=N # population
        self.G=G # generations
        self.BasinName=BasinName
        #------------------------------
        # Read text file of static parms and selected gene sets
        self.ActivePars=OrderedDict([])
        F=open('GeneControl.txt','r')
        for line in F:
            if line.startswith('#'): continue #comment lines
            line=line.split(',')
            self.ActivePars[line[0]]=int(line[1])
            if int(line[1])==0:
                try: setattr(self,line[0],float(line[2]))
                except:
                    print line
                    exit('Error reading gene file')
        F.close()
        #---------
        # save parameter headers being used:
        FF=open('GeneHeader.txt','w') #matches genes.txt
        FF.write('Date,Iteration,')
        for Pari in self.ActivePars.iteritems():
            if int(Pari[1])==1:
                FF.write(Pari[0]+',')
            elif int(Pari[1])==2:
                FF.write(Pari[0]+',')
                FF.write(Pari[0]+'_1,')
        FF.close()
        #---------
        self.n=sum(self.ActivePars.values()) #simple means of dynamic gene population size
        print self.n
        #self.p_c = 0.9 # Crossover probability.9 #not used
        self.p_m = 1./self.n # Mutation propability; set to 1/L, where L=#Decision Variables... ~1 gene mutated each cross-over
        #---------------------
        self.START=datetime.now()
        print "Create begining population"
        for i in range(0, self.N):
            IS=Individual(self)
            self.P.append(IS)
            if self.DEBUG: print i, IS.genes
        print 'Done initializing population...'
    def fast_nondominated_sort(self, P):
        '''
        P is actually R=P+Q being passed in.
        This uses p values to assess dominance in objective space
        '''
        global F
        F = []
        S = dict()
        for p in P:
            S[hash(p)] = []
            p.n = 0 #number of times gene set dominated
            for q in P: #check against all other gene sets
                if p.dominated(q):
                    S[hash(p)].append(q)
                elif q.dominated(p):
                    p.n += 1
                #else:
                    #print 'Non-DOMINATED', q.genes
            if p.n == 0: #if no one dominates it...
                p.rank = 1
                F.append([]) #F is a list of non-dominated points
                F[0].append(p)
        i = 0
        while len(F[i]) != 0:
            Q = []
            for p in F[i]:
                for q in S[hash(p)]:
                    q.n -= 1
                    if q.n == 0:
                        q.rank = i + 1
                        Q.append(q)
            i += 1
            F.append([])
            F[i] = Q #store list of fronts
        return F

    def crowding_distance_assigment(self, X):
        l = len(X)
        for x in X:
            print "Initialize distance for each individual"
            x.distance = 0
        for m in range(0, self.M):
            print "Sort using objective m"
            X.sort(lambda x,y: cmp(x.func_vals[m], y.func_vals[m]))
            "Boundary points are always selected"
            X[0].distance = PosInf
            X[l - 1].distance = PosInf
            global val_list
            val_list=[]
            for i in range(0,l):#JQ Correction: added fmax and fmin parts
                val_list.append(X[i].func_vals[m])
            fmax=max(val_list)
            fmin=min(val_list)
            for i in range(1, l - 1): #JQ Correction: changed 2 to 1
                X[i].distance += (X[i + 1].func_vals[m] - X[i - 1].func_vals[m])/(fmax-fmin)
    
    @staticmethod
    def crowded_comparasion_operator(x,y):
        #print "Because we rank in descending order the return values are inverted"
        if x.rank < y.rank or (x.rank == y.rank and x.distance > y.distance):
            return +1
        else:
            return -1

    def experiment(self):
        """
        This method runs the whole experiment
        """
        for iteration in range(0, self.G):
            self.iteration = iteration
            print "iteration =", iteration, 'of ', self.G
            self.Q = []
            while len(self.Q) < self.N:
                p1 = choice(self.P)
                p2 = choice(self.P)
                while hash(p1) == hash(p2): #ensure no self-mating
                    p2 = choice(self.P)
                self.Q.extend(p1.crossover(p2)) #make children from parent sets
            for c in self.Q:
                c.mutation()
            R = []
            R.extend(self.P)
            R.extend(self.Q)
            F = self.fast_nondominated_sort(R) #returns a list of various genes in groups of fronts
            self.P = []
            i = 0
            while len(self.P) + len(F[i]) <= self.N:
                self.crowding_distance_assigment(F[i])
                self.P.extend(F[i])
                i += 1
            #we now have a list of individs >= N; sort last front i by crowding, then trim:
            F[i].sort(Problem.crowded_comparasion_operator) #only needed for trimmed fronts
            self.P.extend(F[i][0 : self.N-len(self.P)]) #trim to N
        #------------------------------------------------
        print "Show non-dominated fronts"
        num=0
        for f in F:
            if len(f) > 0:#JQ: changed to one to prevent single pt files ### Put back to ZERO for testing
                print "Front (q:",len(f),") = "
                data=zeros([len(f),self.n]) # 
                i=0
                for g in f:
                    data[i,:]=array(g.genes)
                    print around(g.func_vals,5)
                    print data[i,:] #OUTPUT to SCREEN
                    i+=1
                savetxt("Decision"+repr(num).zfill(3)+'.dat',data, fmt='%.5f')
            num+=1
        
        print "Put fronts into files"
        num = 0
        for f in F:
            if len(f) > 0:#JQ: changed to one to prevent single pt files  ### Put back to ZERO for testing
                file = open("front" + repr(num).zfill(3) + ".dat", "w")
                for g in f:
                    t = str(round(g.func_vals[0],5))+" "+str(round(g.func_vals[1],5))+" "+str(round(g.func_vals[2],5))+"\n" # ### UPDATE for number of Obj Functions!
                    file.write(t)
                file.close()
            num += 1
        #-------
        END=datetime.now()#.isoformat()
        print 'Started',self.START.isoformat()
        print 'Ended',END.isoformat()
        Delta=END-self.START
        
        file=open("OutAll.txt",'a')
        for g in self.P:
            #for g in f:
            file.write("%d,%f,%f,%f"%(self.N,round(g.func_vals[0],4),round(g.func_vals[1],4),round(g.func_vals[2],4)))
            for gi in g.genes:
                file.write(",%f"%round(gi,3))
            file.write(",%d\n"%Delta.seconds)
        file.close()
        #-------
        #F=open('RunTimes.txt','a')
        #F.write(str(self.N)+','+str(self.G)+','+str(Delta.seconds)+'\n')
        #F.close()
#------------------------------------------------------------------------
class ProblemModel(Problem):
    
    def UnNorm(self,GName,GVal):
        '''
        This takes the genes, constrained between min and max, and uses the upper/lower
        bounds to convert back to 'normal' values required for the model.
        For NASA SAC-SNOW, gene bounds are [0-1] 
        '''
        Delta=self.VMax[GName]-self.VMin[GName]
        return self.VMin[GName]+GVal*Delta
    
    def SlopeUnNorm(self,GName,GVal):
        '''
        Slopes are bound by difference from (max - min) * 2
        0 = min-max
        1 = max-min
        add check to keep all values within max to min bounds
        '''
        Delta=self.VMax[GName]-self.VMin[GName]
        Value= -Delta +GVal*(2.*Delta)
        #Value= -Delta/2. + GVal*Delta #alternate to limit slope to 1/2 range
        return Value
    
    def f1(self, x):
        '''
        "Objective function 1 for Nash-Sutcliffe Response"
        This first objective calls the model, then finds fitness
        '''
        #-----------------------
        print 'Updating Parameters File'
        # GENES: uztwm,uzfwm,lztwm,lzfpm,lzfsm,uzk,lzpk,lzsk,zperc,rexp,pfree,scf,mfmax,mfmin,uadj,pxtemp
        #Only updating certain genes - use genes list to create the parameter lists passed in to be updated
        
        Pars=OrderedDict([]) #start with an empty dictionary
        i=0 #gene index
        for Pari in self.ActivePars.iterkeys():
            if self.ActivePars[Pari]==0:
                Pars[Pari]=self.__getattribute__(Pari)
                Pars[Pari+'_1']=0. #no 'slope'
            elif self.ActivePars[Pari]==1:
                Pars[Pari]=self.UnNorm(Pari,x.genes[i])
                if not Pari.startswith('Beta'):
                    Pars[Pari+'_1']=0. #no 'slope'
                i+=1 #just an intercept term
            elif self.ActivePars[Pari]==2:
                Pars[Pari]=self.UnNorm(Pari,x.genes[i])
                Pars[Pari+'_1']=self.SlopeUnNorm(Pari,x.genes[i+1])
                i+=2 #intercept and a slope term
            else:
                print Pari
                exit('bad key:value in ActivePars - must be in set [0,1,2]')

        #print Pars
        globals()['Pars']=Pars
        #global parameter Pars gets updated during evaluate - write the unnormalized genes out
        FF=open('genes_unnorm.txt','a')
        for GID,Gi in Pars.iteritems():
          FF.write("%9.3f"%(Gi))
        FF.write('\n')
        FF.close()
        
        # pass this dictionary in to update files:
        DeckMod(self,Pars) #
        
        #print 'Running SAC-SNOW....!'
        subprocess.call(r"mcp3 drgc2.out OUT",shell=True)
        
        #Find fitness, finally...:
        self.f1v=Fit1(self)
        return self.f1v
    
    def f2(self,x):
        '''
        "Objective function 2 for Volume"
        '''
        # Models Already Run - just grab the returned fitness value for Volume
        self.f2v=Fit2(self)
        return self.f2v
    
    
    def f3(self,x):
        '''
        "Objective function to evaluate parameter decision space"
        '''
        #this information was collected based on 'expert knowledge' of relationships between parameters
        self.f3v=Fit3(self,x.genes,Pars) #pass Pars or x.genes
        return self.f3v
        
    #----------------------------------------
    obj_funcs = [f1, f2, f3] #list of objective FUNCTIONS
    if len(obj_funcs) != Problem.M: #
		exit('Update number of obj_funcs to include!')
########################################
if __name__ == "__main__":
    file=open("OutAll.txt",'w') #clear output file
    file.close()
    G=70 # Number of generations
    BasinName='drgc2' 
    for N in [70]: #change population size 10,20,30,  40
        p1 = ProblemModel(N,G,BasinName)
        p1.experiment()

