#!/usr/bin
import math
from fpconst import *
from random import uniform, random, betavariate, normalvariate
from copy import *
from numpy import around
from datetime import datetime
import os
#from Problem import *
from numpy import savetxt,arange

#__all__ = ['Individual']

"""
This class contains definition of single individual in population.
"""
class Individual():
    "Some properties"
    rank = PosInf
    distance = PosInf
    genes = []
    func_vals = []
    
    print "Initialize completly new individual, all genes are randomly choosen."    
	#Initialize empty child for crossover.
    def __init__(self, problem, empty = False):
        if problem is None or not isinstance(problem.obj_funcs, list) or len(problem.obj_funcs) < 2:
            raise TypeError('Problem passed to constructor has to have obj_funcs')
        
        "Store the problem to get functions list for later use."
        self.problem = problem
        
        self.genes = []
        if empty is False:
            #print "Prepare completly new individual"
            for g in range(problem.n):
                v = uniform(problem.var_bounds[0], problem.var_bounds[1])
                self.genes.append(v)
            "Evaluate objective functions for new individuals"
            # self.evaluate()
        else:
            #print "Empty individual"
            for g in range(problem.n):
                v = PosInf
                self.genes.append(v)
        
    """
    Evaluate objective functions for new individuals
    """        
    def evaluate(self):
        #print 'Inside EVALUATE'
        self.func_vals = []
        for f in self.problem.obj_funcs:
            fv = f(self.problem,self) #calls f1 and then f2, etc. (self.problem,self)
            self.func_vals.append(fv)
    
    """
    This method guarantee that any gene does not cross boundaries defined in problem
    """        
    def normalize(self):
        #print 'Inside NORMALIZE'
        for i in range(0, len(self.genes)):
            if self.genes[i] > self.problem.var_bounds[1]:
                "When value of gene crosses upper boundary set it to that boundary value"
                self.genes[i] = self.problem.var_bounds[1]
            elif self.genes[i] < self.problem.var_bounds[0]:
                "When value of gene crosses lower boundary set it to that boundary value"
                self.genes[i] = self.problem.var_bounds[0]
    """
    This method realizes the crossover for new individual (SBX).
    Result of the method is a list with two childrens.
    """
    def crossover(self, second_parent):
        childs = [Individual(self.problem, True), Individual(self.problem, True)]
        print ' In the CROSSOVER method'
        j = 0;
        for g in self.genes:
            u = random()
            if u <= 0.5: #bq<1
                bq = pow((2.0 * u), (1.0 / (self.problem.eta_c + 1.0)))
            else: #bq>=1
                bq = 1.0 / pow((2.0 * (1.0 - u)), (1.0 / (self.problem.eta_c + 1.0)))
                
            ch1v = 0.5 * (((1.0 + bq) * g) + (1.0 - bq) * second_parent.genes[j])
            childs[0].genes[j] = ch1v # add new gene for child 1
            ch2v = 0.5 * (((1.0 - bq) * g) + (1.0 + bq) * second_parent.genes[j])
            childs[1].genes[j] = ch2v # add new gene for child 2 
            #print "ch1v [",j,"] = ", ch1v, "1par_g", g , " 2par_g = ", second_parent.genes[j], " bq = ", bq 
            j += 1
            
        childs[0].normalize() #make sure genes are bounded correctly
        #childs[0].evaluate() #populates self.func_vals
        childs[1].normalize() #make sure genes are bounded correctly
        #childs[1].evaluate() #populates self.func_vals, again
        #print 'Skipping Cross-over evaluation'
        #--
        return childs
    
    def mutation(self):
        """
        This method realizes the mutation for new individual (polynomial mutation).
        Result of the method is the individual itself (was: 'is new individual') after mutation.
        """
        child = self # was deepcopy(self)
        for i in range(0, len(child.genes)): #iterate through all genes of set individually
            u = random()
            g = child.genes[i]
            if u <= self.problem.p_m:
                r = random()
                if r < 0.5:
                    delta = pow(2.0 * r, (1.0 / (self.problem.eta_m + 1.0))) - 1.0
                else:
                    delta = 1 - pow(2.0 * (1.0 - r), (1.0 / (self.problem.eta_m + 1.0)))
                child.genes[i] = g + delta * (self.problem.var_bounds[1] - self.problem.var_bounds[0])    
        child.normalize() #check bounds
        child.evaluate() #get objective values
        # Save current result AFTER evaluation:
        
        if os.path.isfile('./fitness'+str(os.getpid())+'.txt'):
            FF=open('./fitness'+str(os.getpid())+'.txt','a')
            FF.write(str(datetime.now())+',%d'%(self.problem.iteration))
            [FF.write(',%.5f'%(Fi)) for Fi in self.func_vals] #savetxt(FF,[self.func_vals],fmt='%.5f',delimiter=',')
            FF.write('\n')
            FF.close()
        else:
            FF=open('./fitness'+str(os.getpid())+'.txt','w')
            FF.write(str(datetime.now())+',%d'%(self.problem.iteration))
            [FF.write(',%.5f'%(Fi)) for Fi in self.func_vals] #savetxt(FF,[self.func_vals],fmt='%.5f',delimiter=',')
            FF.write('\n')
            FF.close()
        if os.path.isfile('./genes'+str(os.getpid())+'.txt'):
            ff=open('./genes'+str(os.getpid())+'.txt','a')
            ff.write(str(datetime.now())+',%d'%(self.problem.iteration))
            [ff.write(',%.5f'%(Fi)) for Fi in self.genes] #genes=[self.genes] #savetxt(ff,genes,fmt='%.5f',delimiter=',')
            ff.write('\n')
            ff.close()
        else:
            ff=open('./genes'+str(os.getpid())+'.txt','w')
            ff.write(str(datetime.now())+',%d'%(self.problem.iteration))
            [ff.write(',%.5f'%(Fi)) for Fi in self.genes] #genes=[self.genes] #savetxt(ff,genes,fmt='%.5f',delimiter=',')
            ff.write('\n')
            ff.close()
        return child
    
    """
    Method checks whether the individual is dominated  other.
    """
    def dominated(self, other):
        dominated = False
        #print "Inside Dominated in Individual"
        #for i in range(0, len(self.genes)):
        #    if self.genes[i] < other.genes[i]:
        #        dominated = True
        #print "self = ",self,"other = ",other
        #print "self.genes = ", self.genes, "other.genes = ", other.genes
        
        #for i in range(0, len(self.func_vals)):
            #print "self.func_vals[",i,"]",self.func_vals[i], "other.func_vals[",i,"]",other.func_vals[i]
        #    if self.func_vals[i] > other.func_vals[i]:
        #        dominated = True
        flag = [False, False]
        for i in range(0, len(self.func_vals)):
            if self.func_vals[i] < other.func_vals[i]:
                flag[0] = True
            elif self.func_vals[i] > other.func_vals[i]:
                flag[1] = True
        if flag[0] == True and flag[1] == False:
            dominated = True 
        return dominated
