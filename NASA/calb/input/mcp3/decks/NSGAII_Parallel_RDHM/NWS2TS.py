#!/usr/bin
'''
This script reads an NWS Card file and outputs a text file.
Assumes delta-time in the header is HOURS.

Call script with two arguments 1) file to be read, 2) output file.

Jonathan Quebbeman
February 3, 2016
'''

#from sys import argv
from numpy import mod,savetxt,floor,arange

#FileName=argv[1]
#OutFile=argv[2] #originally wrote to a new text file; now return a pandas DF

def NWS2TS(FileName):
  Orig=open(FileName,'r')
  OutFile=FileName.split('.')[0]+'.tsnsga'
  New=open(OutFile,'w')
  New.write('Year,Month,Day,Hour,Value\n')
  #days of month:
  MonDays={1:31,2:28,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31}
  MonDaysLeap={1:31,2:29,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31}
  
  #for line in Orig:
  Reci=0 #record index keeper for flow values
  i=0 #index in file being read
  Stepi=0 #index for counting values in a month
  line=Orig.readline()
  while line:
    if line.startswith('$'):
      line=Orig.readline()
      continue
    elif line.startswith('DATACARD'): #grab info from 'DATACARD' line
      DeltaT=int(line.split()[4])
      assert mod(24,DeltaT)==0, 'DeltaT value must be multiple of 24!'
      ReadFormat=True #know next line has format data - use a switch
    elif ReadFormat:#grab start-end dates and format data
      Row=line.split()
      StartMonth=int(Row[0])
      StartYear=int(Row[1])
      EndMonth=int(Row[2])
      EndYear=int(Row[3])
      NumDataCols=int(Row[4])
      Format=Row[5]
      #---
      CurYear=StartYear
      CurMonth=StartMonth
      CurDay=1
      CurHour=0
      ReadFormat=False #turn off - now we can read data
      
    else: #now start reading the data...
      if mod(CurYear,4)==0: #is it a leap year?
        MLen=MonDaysLeap[CurMonth]*(24/DeltaT) #assumed DeltaT is always in hours
      else: #non-leap year
        MLen=MonDays[CurMonth]*(24/DeltaT) #assumed DeltaT is always in hours, and a whole fraction of 24
      
      Row=line.split()
      for Ci in arange(-min(NumDataCols,MLen-Stepi),0): #read just data cols, but reading fewer on partial rows at end of month
        try: Di=Row[Ci] #data value i, from the row with column index Ci
        except:
          print 'Row:',Row
          print 'Ci:',Ci,'Stepi:',Stepi
          print 'Year:',CurYear,'Month:',CurMonth,'Day:',CurDay
        New.write(str(CurYear)+','+str(CurMonth)+','+str(CurDay)+','+str(CurHour)+','+Di+'\n')
        
        Reci+=1 #Update row index 
        CurDay+=int(floor(DeltaT/24.))
        if mod(CurHour,24)==0:
          CurHour=0
        else:
          CurHour+=DeltaT
        Stepi+=1
      if Stepi>=MLen: #reset month
        CurMonth+=1
        CurDay=1
        Stepi=0
        if CurMonth==13:#reset year
          CurMonth=1
          CurYear+=1
          CurDay=1

    line=Orig.readline()
    #if i==90:
    #  break #for testing
    i+=1
  Orig.close()
  New.close()
  return #New
###################################
'''
print 'Running Sample Data'  
#NWS2TS('/projects/NASA/calb/data/area_ts/QME/DRGC2.QRD5ZZZ')
NWS2TS('/projects/NASA/calb/data/area_ts/SQME/DRGC2.QRDMZZZ')
'''

