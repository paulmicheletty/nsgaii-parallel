#!/usr/bin/python env

'''
python mcp3mod.py drgc2.init

History:
February 3, 2016 JAQ - Original development
February 17, 2016 - Add ADC update function
March 30, 2016 - removed ADC udpate function
May, 2016 - add variable sloped linear band option
'''

from sys import argv
from time import sleep
from numpy import genfromtxt,arange,exp
from scipy.stats import beta
#-----------------------
#-----------------------------------------------

#-----------------------------------------------
#-----------------------------------------------
def DeckMod(self,Pars):
    #-----------------------------------------------
    def CheckVal(GName,Val):
        '''
        Checks to be sure projected values fall within the
        acceptable min-max range.
        '''
        Val=max(Val,self.VMin[GName])
        Val=min(Val,self.VMax[GName])
        return Val

    #open files to write:
    MCP=open(self.BasinName+'.curr','r')
    Out=open(self.BasinName+'.out','w')

    INTRO=True #switch for grabbing some header info

    Names=[] #record names of subbasins/bands to be run

    #-----------------------------------------------
    line=MCP.readline()
    while INTRO:
        Row=line.split()
        try:
          if Row[1][:3]=='MAP':
            Names.append(Row[0])
        except:
            pass
        Out.write(line)
        line=MCP.readline()
        if line.startswith('END'):
            INTRO=False
            Out.write(line)
    #print Names 
    #-----------------------------------------------
    #Now we have names of basins (assuming each basin has a MAP specified)
    line=MCP.readline()
    while line and not INTRO:
        #------------
        if line.startswith('SNOW-17'):

            CurLoc=line.split()[1] #get current basin/band name
            Band=Names.index(CurLoc) # set [0,1,2]
            
            #ParFile=genfromtxt('./inputs/SNOW_'+CurLoc+'.txt',dtype=[('par','S8'),('val','float')])
            #Pars=dict(zip(ParFile['par'],ParFile['val']))
            Out.write(line) # SNOW-17 ...
            for i in range(8): #write out next block of code, no changes
                Out.write(MCP.readline()) #these are the Area-Depletion Curve data...
            #now we can write out SNOW param lines:
            #print Pars['scf']#,Pars['mfmax'],Pars['mfmin'],Pars['mfmin'],Pars['uadj'],(Pars['si'])
            
            Out.write("{:5.2f}".format(CheckVal('scf',Pars['scf']+Band*Pars['scf_1']))+\
            "{:5.2f}".format(CheckVal('mfmax',Pars['mfmax']+Band*Pars['mfmax_1']))+\
            "{:5.2f}".format(CheckVal('mfmin',Pars['mfmin']+Band*Pars['mfmin_1']))+\
            "{:5.2f}".format(CheckVal('uadj',Pars['uadj']+Band*Pars['uadj_1']))+\
            "{:4.0f}".format(CheckVal('si',Pars['si']+Band*Pars['si_1']))+'. '+'\n')
            Out.write("{:5.2f}".format(CheckVal('nmf',Pars['nmf']+Band*Pars['nmf_1']))+\
            "{:5.2f}".format(CheckVal('tipm',Pars['tipm']+Band*Pars['tipm_1']))+\
            "{:5.2f}".format(CheckVal('mbase',Pars['mbase']+Band*Pars['mbase_1']))+\
            "{:5.2f}".format(CheckVal('pxtemp',Pars['pxtemp']+Band*Pars['pxtemp_1']))+\
            "{:5.2f}".format(CheckVal('plwhc',Pars['plwhc']+Band*Pars['plwhc_1']))+\
            "{:5.2f}".format(CheckVal('daygm',Pars['daygm']+Band*Pars['daygm_1']))+'\n')
            if self.ActivePars['Beta10']==1 and self.ActivePars['Beta20']==1: #check first level only
                # Make Beta Curves
                x=arange(0.1,1,.1) #0.1-0.9, nine values
                if Band==0:
                    B=beta.cdf(x,Pars['Beta10'],Pars['Beta20'])
                elif Band==1:
                    B=beta.cdf(x,Pars['Beta11'],Pars['Beta21'])
                else:
                    B=beta.cdf(x,Pars['Beta12'],Pars['Beta22'])
                    
                for Bi in B:
                    Out.write("{:5.2f}".format(max(Bi,0.05))) #can't have values <0.05 (first 0% set to 0.05, and must be increasing)
                Out.write('\n')
                #read three lines, don't write (because we just did above):
                for i in range(3): #for beta terms
                    MCP.readline()
            else: # if not using betas:
                for i in range(2): #read two lines, don't write (because we just did above)
                    MCP.readline()
            #now we can continue reading/writing lines with else statement
        #------------
        elif line.startswith('SAC-SMA'):
            CurLoc=line.split()[1] #get current basin/band name
            Band=Names.index(CurLoc) # set [0,1,2]
            #ParFile=genfromtxt('./inputs/SAC_'+CurLoc+'.txt',dtype=[('par','S8'),('val','float')])
            #Pars=dict(zip(ParFile['par'],ParFile['val']))
            Out.write(line)
            for i in range(2): #write out next block of code, no changes
                Out.write(MCP.readline())
            Out.write('                   '+\
            "{:5.2f}".format(CheckVal('pxadj',Pars['pxadj']+Band*Pars['pxadj_1']))+\
            "{:5.2f}".format(CheckVal('peadj',Pars['peadj']+Band*Pars['peadj_1']))+\
            "{:6.1f}".format(CheckVal('uztwm',Pars['uztwm']+Band*Pars['uztwm_1']))+\
            "{:5.1f}".format(CheckVal('uzfwm',Pars['uzfwm']+Band*Pars['uzfwm_1']))+\
            "{:5.2f}".format(CheckVal('uzk',Pars['uzk']+Band*Pars['uzk_1']))+\
            "{:5.1f}".format(CheckVal('pctim',Pars['pctim']+Band*Pars['pctim_1']))+\
            "{:5.2f}".format(CheckVal('adimp',Pars['adimp']+Band*Pars['adimp_1']))+\
            "{:5.1f}".format(CheckVal('riva',Pars['riva']+Band*Pars['riva_1']))+\
            "{:9.2f}".format(CheckVal('efc',Pars['efc']+Band*Pars['efc_1']))+'\n')
            Out.write('                    '+\
            "{:5.1f}".format(CheckVal('zperc',Pars['zperc']+Band*Pars['zperc_1']))+\
            "{:5.1f}".format(CheckVal('rexp',Pars['rexp']+Band*Pars['rexp_1']))+\
            "{:5.1f}".format(CheckVal('lztwm',Pars['lztwm']+Band*Pars['lztwm_1']))+\
            "{:5.1f}".format(CheckVal('lzfsm',Pars['lzfsm']+Band*Pars['lzfsm_1']))+\
            "{:5.1f}".format(CheckVal('lzfpm',Pars['lzfpm']+Band*Pars['lzfpm_1']))+\
            "{:5.2f}".format(CheckVal('lzsk',Pars['lzsk']+Band*Pars['lzsk_1']))+\
            "{:5.3f}".format(CheckVal('lzpk',Pars['lzpk']+Band*Pars['lzpk_1']))+\
            "{:5.1f}".format(CheckVal('pfree',Pars['pfree']+Band*Pars['pfree_1']))+\
            "{:4.2f}".format(CheckVal('rserv',Pars['rserv']+Band*Pars['rserv_1']))+\
            "{:6.1f}".format(CheckVal('side',Pars['side']+Band*Pars['side_1']))+'\n')
            for i in range(2): #read two lines, don't write (because we just did above)
                MCP.readline()
        else:
            Out.write(line)
        line=MCP.readline()
  
    #Close out:
    MCP.close()
    Out.close()

#------------------------------------
from Fitness import * 

class TestPars(object):
    BasinName='drgc2'
    DeckMod=DeckMod
    Fit1=Fit1
    Fit2=Fit2
    Fit3=Fit3
    def MakePars(self):
        '''
        This is used to make a dummy Pars dictionary to call the routine abs
        '''
        from collections import OrderedDict
        uztwm,uzfwm,lztwm,lzfpm,lzfsm,uzk,lzpk,lzsk,zperc,rexp,\
        pfree,scf,mfmax,mfmin,uadj,si,pxtemp=\
        25.0,34.895,199.2,199.04,140.97,0.3863,0.004234,0.06689,20.0,1.4378,0.25475,1.1706,1.3619,\
        0.3705,0.19963,2444.0,1.53886
        Pars=OrderedDict([('uztwm',uztwm),\
             ('uzfwm',uzfwm),\
             ('lztwm',lztwm),\
             ('lzfpm',lzfpm),\
             ('lzfsm',lzfsm),\
             ('adimp',0.09),\
             ('uzk',uzk),\
             ('lzpk',lzpk),\
             ('lzsk',lzsk),\
             ('zperc',zperc),\
             ('rexp',rexp),\
             ('pctim',0.005),\
             ('pfree',pfree),\
             ('riva',0.0),\
             ('side',0.0),\
             ('rserv',0.3),\
             ('scf',scf),\
             ('mfmax',mfmax),\
             ('mfmin',mfmin),\
             ('uadj',uadj),\
             ('si',si),\
             ('pxtemp',pxtemp),\
             ('nmf',0.2),\
             ('tipm',.2),\
             ('mbase',0.),\
             ('plwhc',0.05),\
             ('daygm',0.),\
             ('pxadj',1.),\
             ('peadj',1.),\
             ('efc',0.2)]) 
        #return Pars
        self.Pars=Pars
        return
        
    def DoWrite(self):
        self.DeckMod(self.Pars)
        return
    
    def PrintFitness(self):
        print self.Fit1()
        print self.Fit2()
T=TestPars()
#T.MakePars()
#T.DoWrite()
T.PrintFitness()


