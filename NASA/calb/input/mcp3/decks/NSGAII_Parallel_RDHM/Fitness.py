#!/usr/bin
import pandas as ps
from numpy import genfromtxt,mean,arange,log, corrcoef, NaN
from numba import jit
from NWS2TS import NWS2TS
from math import isnan
import os
import glob

'''
This contains the fitness functions used for the NSGA model
'''
@jit
def NashSut(Obs,Mod,LOG=False):
    '''
    Nash1970
    '''
    if LOG:
      Obs=log(Obs)
      Mod=log(Mod)
    NS=1.-sum((Obs-Mod)**2) / sum((Obs-mean(Obs))**2)
    if isnan(NS): NS=-999.
    return NS
    

def Kling(Obs,Mod,LOG=False):
    '''
    Kling-Gupta Measure of Efficiency
    Obs and Mod - arrays
    '''
    if LOG:
      Obs=log(Obs)
      Mod=log(Mod)
    Bias=Mod.mean()/Obs.mean()
    Var=(Mod.std()/Mod.mean()) / (Obs.std()/Obs.mean())
    R2=corrcoef(Obs,Mod)[0,1]
    KG=1.-((R2-1.)**2+(Var-1.)**2+(Bias-1.)**2)**.5
    return KG
    
def PercentBias(Obs,Mod):
    '''
    This looks at the absolute value of percent of bias on a monthly basis.
    Optionally, more weight can be applied to high-runoff seasons than dry periods. 
    '''
    Bias=sum(abs((Obs-Mod)/Obs))
    return Bias

@jit
def Shoulder(LP,UP,Pari,Pars,VMin,VMax,PEN=20.): #NEW
  '''
  LP and UP are when penalties START to occur if exceeded.
  Linear scale between parameter bounds and then LP and UP.
  '''
  Pen=0.
  if Pars[Pari]<LP:
    Pen+=(LP-Pars[Pari])/(LP-VMin[Pari])*PEN
  elif Pars[Pari]>UP:
    Pen+=(Pars[Pari]-UP)/(VMax[Pari]-UP)*PEN
  return Pen
'''
@jit
def Shoulder(LP,UP,Pari,Genes,PEN=20.): #OLD
  ''
  This uses the array of genes [0,1] and the parameter
  ID to define triangular shoulder penalties as values
  approach 0 or 1, with no penalty between shoulders. 
  ''
  Pen=0.
  if Genes[Pari]<LP:
    Pen+=(LP-Genes[Pari])/LP*PEN #; print 'Sa',Pari
  elif Genes[Pari]>UP:
    Pen+=(Genes[Pari]-UP)/(1.-UP)*PEN #; print 'Sb',Pari
  return Pen
'''
#----------------------------------------------
#----------------------------------------------
def Fit1(self):
    '''
    # NASH-SUTCLIFFE
    Could have start, end and delta t as a global variable in case different runs
    use different lengths of observed or simulated data. Hard coded below.
    Returns: scalar, for minimization problems, we want to minimize the negative Nash-Sut.
    '''
    output_folder = self.Basin+'_'+str(os.getpid())
    print('within fitness function 1: pid ==>')
    print output_folder
    startDate = '10-1-1988 00:00:00'
    F=open(os.path.join(output_folder,self.Basin+'_discharge_outlet.ts'),'r')
    for i,line in enumerate(F):
        if line.startswith('$'):
            comment = i
    F.close()
    mod_data = genfromtxt(os.path.join(output_folder,self.Basin+'_discharge_outlet.ts'), skip_header=comment+3, dtype=float)
    dates = ps.date_range(startDate, periods = len(mod_data[:,3]), freq = '3H')
    Mod = ps.DataFrame(index=dates, columns=['Mod']) 
    Mod['Mod'] = mod_data[:,3]
    Mod['Mod'] = Mod['Mod'].mul(3.2808**3) #cms to cfs
    obs_filename = glob.glob('/projects/NASA/hisobs/'+self.Basin+'*QINE')
    Obs = ps.read_table(obs_filename[0], index_col = 0, parse_dates = True, sep = ',', skiprows=3,header = None)
    Obs.rename(columns={1: 'Obs'}, inplace=True)
    Obs = Obs.resample('3h').mean() # Resample to 3 hour
    #find concurrent period for both model and observed record:
    START=max(Obs.index[0],Mod.index[0])
    END=min(Obs.index[-1],Mod.index[-1])
    
    #Trim to common length:
    self.Obs_Trim=Obs[(Obs.index>=START)&(Obs.index<=END)]
    self.Obs_Trim[self.Obs_Trim<0]=NaN
    self.Mod_Trim=Mod[(Mod.index>=START)&(Mod.index<=END)]
    
    # Merge into DF, Find NS Stats:
    DF=ps.DataFrame(self.Obs_Trim).join(self.Mod_Trim).dropna() #put common length data into DF
    self.DF=DF

    KG=0.5*Kling(DF.Obs.values,DF.Mod.values) + 0.5*Kling(DF.Obs.values,DF.Mod.values,LOG=True)

    ''' #OLD NASH-SUTCLIFFE
    GB=self.DF.groupby(self.DF.index.year) #group by the years to do stats annually, append to DFNS
    DFNS=ps.DataFrame(columns=['NSUO','NSLO','NSUM','NSLM']) #make an empty DF to store results
    #DFNS=ps.DataFrame(columns=['NSUO','NSLO'])#,'NSUM','NSLM']) #make an empty DF to store results
    for Y,Gi in GB:
        #create selection arrays:
        SelUO=Gi.Obs>Gi.Obs.quantile(.95)
        SelUM=Gi.Mod>Gi.Mod.quantile(.95)
        SelLO=Gi.Obs<Gi.Obs.quantile(.30)
        SelLM=Gi.Mod<Gi.Mod.quantile(.30)
        #Find NS
        try:
            # GHP: Fixed Index name
            DFNS.loc[Y,'NSUO']=NashSut(Gi.Obs[SelUO].values , Gi.Mod[SelUO].values)
            DFNS.loc[Y,'NSLO']=NashSut(Gi.Obs[SelLO].values , Gi.Mod[SelLO].values)
            #DFNS.loc[Y,'NSLO']=NashSut(log(Gi.Obs)[SelLO].values , log(Gi.Mod)[SelLO].values)
            DFNS.loc[Y,'NSUM']=NashSut(Gi.Obs[SelUM].values , Gi.Mod[SelUM].values)
            DFNS.loc[Y,'NSLM']=NashSut(Gi.Obs[SelLM].values , Gi.Mod[SelLM].values)
            #DFNS.loc[Y,'NSLM']=NashSut(log(Gi.Obs)[SelLM].values , log(Gi.Mod)[SelLM].values)
        except:
            DFNS.iloc[:,:]=-999. #divide by zero condition; make undesirable
            
    #print DFNS #TEMP
    #DFNS.to_csv('DFNS.txt')
    
    #print DFNS
    NS=NashSut(self.DF.Obs.values,self.DF.Mod.values) # Nash-Sutcliff of whole record
    NS=(NS+DFNS.mean().mean())/2. #find equally weighted NS value
    if isnan(NS): NS=-999.
    #----------------------
    print 'Nash-Sut:',NS
    return -NS
    '''

    return -KG # minimize
#----------------
def Fit2(self):
    '''
    #VOLUME DIFFERENCE
    The instance values for Obs and Mod have already been found. Use them here
    without needing to re-calculate.
    '''
    #Find sum of monthly yields:
    Obs_Mon=self.Obs_Trim.groupby([self.Obs_Trim.index.year,self.Obs_Trim.index.month]).sum() #OR MEAN() ???
    Mod_Mon=self.Mod_Trim.groupby([self.Mod_Trim.index.year,self.Mod_Trim.index.month]).sum()
    rng=ps.date_range(self.Mod_Trim.index[0],self.Mod_Trim.index[-1],freq='M')
    VDiff=ps.Series(Obs_Mon.Obs.values-Mod_Mon.Mod.values,index=rng) #create series with new monthly index
    VDiffM=VDiff.groupby(VDiff.index.month).mean().abs() # groupby, then find the absolute value of Vol diff.
    #print VDiffM
    return VDiffM.max()

#----------------
def Fit3(self,Genes,Pars):
    '''
    Genes is an array of actual 0-1 gene values
    Pars is the dictionary of un-normalized parameter values
    '''
    Pen=0.
    PEN=20. #Standard Penalty Term
    #for Gi in Genes:
    #  Pen+=abs(Gi-0.5) # try to prevent extreme boundary values

    if Pars['snow_SCF']>1.25:
      Pen+=999. #; print 'V1'
    if Pars['sac_LZFPM']-Pars['sac_LZFSM']<20.:
      Pen+=PEN #; print 'V2'
    if Pars['sac_LZFSM']/(Pars['sac_UZFWM']-9.99)>6.5:
      Pen+=PEN #; print 'V3'
    if Pars['sac_LZFSM']/(Pars['sac_UZFWM']-9.99)<1.66:
      Pen+=PEN #; print 'V4'
    if Pars['sac_LZFPM']/Pars['sac_LZFSM']>8.:
      Pen+=PEN #; print 'V5'
    if Pars['sac_LZFPM']/Pars['sac_LZFSM']<200/300.:
      Pen+=PEN #; print 'V6'
    if Pars['sac_UZK']>0.5-Pars['sac_LZFPM']*0.3/800:
      Pen+=PEN #; print 'V7'
    
    #Shoulder Penalty Funcs:
    Pen+=Shoulder(1250,3750,'snow_SI',Pars,self.VMin,self.VMax,PEN=PEN)# si NEW
    Pen+=Shoulder(1.,1.3,'snow_SCF',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(.1,.4,'snow_MFMIN',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(.6,1.4,'snow_MFMAX',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(.03,.2,'snow_UADJ',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(20.,75.,'sac_UZFWM',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(25.,100.,'sac_UZTWM',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(60.,400.,'sac_LZFPM',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(15.,200.,'sac_LZFSM',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(.03,.15,'sac_LZSK',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(.05,.4,'sac_PFREE',Pars,self.VMin,self.VMax,PEN=PEN)
    Pen+=Shoulder(1.4,3.,'sac_REXP',Pars,self.VMin,self.VMax,PEN=PEN)
    
    #----
    mfmaxmin=Pars['snow_MFMAX']-Pars['snow_MFMIN']
    if mfmaxmin<=0.2:
        Pen+=PEN*5 #increased significance of this penalty - really discourge low diff's
    elif mfmaxmin<0.3:
        Pen+=(0.3-mfmaxmin)/(0.3-0.2)*PEN*5
    #----
    uztlzt=Pars['sac_UZTWM']/Pars['sac_LZTWM']
    if uztlzt>1.: Pen+=PEN
    elif uztlzt>0.75:
        Pen+=(uztlzt-.75)/(1.-.75)*PEN
    #----
    lzrat=Pars['sac_LZFSM']/Pars['sac_LZFPM']
    if lzrat>1.:
        Pen+=PEN*5.
    elif lzrat>.75:
        Pen+=(lzrat-.75)/(1.-.75)*PEN
    #----
    if Pars['sac_LZPK']>0.012 and Pars['sac_LZSK']<0.05:
        Pen+=PEN
    #----
    if Pars['sac_LZSK']>.12 and Pars['sac_UZK']<.25: Pen+=PEN
    if Pars['sac_LZSK']>.17 and Pars['sac_UZK']<.3: Pen+=PEN
    
    return Pen
#----------------
def Fit4(self):
    '''
    NOT USED
    # PEAK MONTHLY FLOW DIFFERENCE
    Grab the maximum value for each month, compare the peaks
    '''
    Obs_Mon=self.Obs_Trim.groupby([self.Obs_Trim.index.year,self.Obs_Trim.index.month]).max()
    Mod_Mon=self.Mod_Trim.groupby([self.Mod_Trim.index.year,self.Mod_Trim.index.month]).max()
    PeakDiff=sum(abs(Obs_Mon.values-Mod_Mon.values))
    return PeakDiff
#-------------------
def Fit5(self,Pars):
    '''
    NOT USED
    this function looks at decision space (rather than objective space), and evalutes
    parameter set deviations from 
    '''  
    Pen=0.
    if Pars['snow_SCF']>1.25: Pen+=1
    if Pars['sac_LZFPM']-Pars['sac_LZFSM']<20.: Pen+=1
    return Pen
  
#
