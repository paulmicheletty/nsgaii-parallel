#!/usr/bin/python env

'''
python rdhm_modpar.py

History:
February 26, 2016 PDM - Original development
'''
from sys import argv
from time import sleep
from numpy import genfromtxt,arange
from datetime import datetime
#from scipy.stats import beta
import os
import re
import subprocess
import sys
from shutil import copyfile
# from shutil import copytree
from distutils.dir_util import copy_tree
########################################
# Define directories and deck ??? Make this an input? How to make flexible?
########################################
DeckName = 'nasa_animas_1k_riverside_rescale_7.deck'
dir = '/projects/NASA/rdhm_sa/'
#xmrg_dir = '/SAN1/home/pdm/playground/xmrgp_test/'
mask='./basins_mask.gz'
xmrg_dir_in = './params1k.co'
DECK_IN=open(DeckName,'r').read()
# out_file=DECK_IN
#########################################
# Define parameter modification functions
#########################################
def initialize_folders(self):
    directory = self.Basin+'_'+str(os.getpid())
    if not os.path.exists(directory):
        print("Initializing Output Folders")
        os.makedirs(directory)
        xmrg = 'xmrg_'+self.Basin+'.1d'
        tair = 'tair_'+self.Basin+'.1d'
        psf = 'psfrac_'+self.Basin+'.1d'
        copyfile(DeckName, os.path.join(directory,DeckName+'.out'))
        copyfile(xmrg, os.path.join(directory,xmrg))
        copyfile(tair, os.path.join(directory,tair))
        copyfile(psf, os.path.join(directory,psf))
        os.makedirs(os.path.join(directory,'params1k.co.calb'))
        copy_tree('params1k.co',os.path.join(directory,'params1k.co.calb'))

# Update parameter values (first in deck if value is positive. if negative, modify xmrg)
def update_par(self,key,Pars):
    global DECK_IN, out_file
    if re.search(key+'=[+-]?\d+(?:\.\d+)?', DECK_IN):
        org_val=float(re.search(key+'=[+-]?\d+(?:\.\d+)?', DECK_IN).group(0).split('=')[1])
        print(org_val)
        new_val=org_val*Pars[key]
        print(new_val)
        ### DEBUG ###
        if self.DEBUG:
            if os.path.isfile('./parameter_values'+str(os.getpid())+'.txt'):
                ff=open('./parameter_values'+str(os.getpid())+'.txt','a')
                ff.write(str(datetime.now()))
                ff.write('original value,'+str(org_val)+',scaler,'+str(Pars[key])+',new value,'+str(new_val)+'\n')
                ff.close()
            else:
                ff=open('./parameter_values'+str(os.getpid())+'.txt','w')
                ff.write(str(datetime.now()))
                ff.write('original value,'+str(org_val)+',scaler,'+str(Pars[key])+',new value,'+str(new_val)+'\n')
                ff.close()
        #############
        if org_val >= 0:
            out_file = re.sub(key+'=[+-]?\d+(?:\.\d+)?', key+'='+str(new_val),out_file)
        else:
            mod_xmrgpar('1',key,Pars,'multiply','None')
    else:
        print("parameter not found in deck file...")
    #     mod_xmrgpar('1',key,Pars,'multiply','None')

## Function to replace non-gridded parameters in deck file
def replace_par(param,deckfile):
    global out_file
    # This syntax searchs based on parameter name and finds the following number and replaces it
    # TODO (ghp): need to read the original value and multiply it by the ratio from Pars
    #               
    a = re.search(param+'=([-+]?\d*\.\d+|\d+)', deckfile)
    b = float(a.groups()[0])*Pars[param]
    out_file = re.sub(param+'=([-+]?\d*\.\d+|\d+)', param+'='+str(b),deckfile)
    #out_file = re.sub(param+'=([-+]?\d*\.\d+|\d+)', param+'='+str(Pars[param]),deckfile)
    print param+' modified in deck file successfully'

## Run commandline arguement that uses xmrgp to change grids
def mod_xmrgpar(MaskID,param,Pars,operation1,operation2):
    """Write new lookup file and modify xmrg param file using xmrgp tool and lookup function.
    This function can apply two operations in sequence if operation2 is not 'None'
    **Arguments:
    ID -- ID of basin in pre-defined in mask file
    param -- parameter name as defined in RDHM [str]
    operation -- Mathmatical operation. Available operations include: multiply, divide, add, subtract, None.
    """
    # Write new lookup.txt for each xmrg param
    with open(os.path.join(xmrg_dir_out,'lookup.txt'), 'wb') as f:
        f.write(MaskID+' '+str(Pars[param]))
    # Run xmrgp command
    cmd=['xmrgp', 'lookup', os.path.join(xmrg_dir_in,param+'.gz'), 
                                mask,
                                os.path.join(xmrg_dir_out,param+'.gz'),
                                os.path.join(xmrg_dir_out,'lookup.txt'),operation1]
    print cmd                            
    process = subprocess.Popen(['xmrgp', 'lookup', os.path.join(xmrg_dir_in,param+'.gz'), 
                                mask,
                                os.path.join(xmrg_dir_out,param+'.gz'),
                                os.path.join(xmrg_dir_out,'lookup.txt'),operation1],
                                stdin=subprocess.PIPE,stderr=subprocess.PIPE)
    output1 = process.communicate()
    if operation2!='None':
        process = subprocess.Popen(['xmrgp', 'lookup', os.path.join(xmrg_dir_out,param+'.gz'), 
                                    mask,
                                    os.path.join(xmrg_dir_out,param+'.gz'),
                                    os.path.join(xmrg_dir_out,'lookup.txt'),operation2],
                                    stdin=subprocess.PIPE,stderr=subprocess.PIPE)
        output2 = process.communicate()
        # If xmrgp commandline process doesn't work- print error; exit script 
        if output2[1]!='':
            print output2[1]
            #sys.exit()
        else:
            print param+' xmrg modified successfully'
    else:
        if output1[1]!='':
            print output1[1]
            #sys.exit()
        else:
            print param+' xmrg modified successfully'
            
#########################################
# Perform parameter modifications 
#########################################

# Read parameter ratio
# ParFile=genfromtxt('/projects/NASA/autocal_rti/models/rdhm_anim/PAR_Ratio.txt',dtype=[('par','S12'),('val','float')])
# Pars=dict(zip(ParFile['par'],ParFile['val']))

# Open deck file
# DECK_IN=open(os.path.join(dir,DeckName),'r').read()
# DECK_OUT=open(os.path.join(dir,DeckName+'.out'),'w')
# Modify/Replace non-gridded parameters in deckfile
# First param uses input deck, others use output so the changes stack
### SNOW-17 PARAMS ###
# out_file=DECK_IN

#GHP: Replace parameter value only if the parameter value in the original deck is positive. 
#      Otherwise, call mod_xmrgpar function 
def RDHM_MOD(self,Pars):
    # Open deck file
    global xmrg_dir_out
    global out_file
    # Intialize
    initialize_folders(self)
    xmrg_dir_out = self.Basin+'_'+str(os.getpid())+'/params1k.co.calb'
    dir_out = self.Basin+'_'+str(os.getpid())
    DECK_IN=open(DeckName,'r').read()
    DECK_OUT=open(os.path.join(dir_out,DeckName+'.out'),'w')
    out_file=DECK_IN
        
    for key in Pars:
        print key
        update_par(self,key,Pars)

    # Write new deck file
    DECK_OUT.write(out_file)
    DECK_OUT.close()
    
    ###Replace output-path in deckfile
    s = open(os.path.join(dir_out,DeckName+'.out'),'r').read()
    s = s.replace('co1k_out_cal', dir_out)
    f = open(os.path.join(dir_out,DeckName+'.out'),'w')
    f.write(s)
    f.close()
    
    print('running rdhm...')
    run = subprocess.Popen(['/projects/NASA/hlrdhm_3.5.10_source_binary_parameters/bin-test/bin/rdhm', '-I', 
                                    os.path.join(dir_out,DeckName+'.out')],
                                    stdin=subprocess.PIPE,stderr=subprocess.PIPE)
    
    run_output = run.communicate()
    # print run_output
#     subprocess.call(r"/projects/NASA/hlrdhm_3.5.10_source_binary_parameters/bin-test/bin/rdhm -I nasa_animas_1k_riverside_rescale_7.deck >> out7.log",shell=True)
